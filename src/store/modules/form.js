export default {
    state: {
        ignoreColumns: ['id', 'created_at', 'updated_at']
    },
    getters: {
        ignoreColumns(state) {
            return state.ignoreColumns;
        }
    }
}
import { HTTP } from "@/common/HTTP";

export default {
    state: {
        orders: [],
        orderDetail: null,
        orderPagination: {
            nextUrl: '/api/v1/orders'
        }
    },
    getters: {
        orders(state) {
            return state.orders;
        },
        orderDetail(state) {
            return state.orderDetail;
        },
        orderPagination(state) {
            return state.orderPagination;
        }
    },
    mutations: {
        addOrders(state, orders) {
            state.orders = state.orders.concat(orders);
        },
        setOrderDetail(state, order) {
            state.orderDetail = order;
        },
        clearOrders(state) {
            state.orders = [];
            state.orderPagination.nextUrl = '/api/v1/orders';
        },
        setNextURL(state, url) {
            state.orderPagination.nextUrl = url;
        }
    },
    actions: {
        async findOrderById({ commit }, id) {
            let response = await HTTP.get(`/api/v1/orders/${id}`);
            commit('setOrderDetail', response.data);
        },
        async fetchOrders({ commit, getters }) {
            let response = await HTTP.get(getters.orderPagination.nextUrl);

            commit('addOrders', response.data.data);
            commit('setNextURL', response.data.next_page_url);
        },
        async updateOrder(context, order) {
            await HTTP.put(`/api/v1/orders/${order.id}`, order);
        }
    }
}
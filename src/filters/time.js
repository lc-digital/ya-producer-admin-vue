import Time from "@/helpers/time";

export const time = value => {
    if (!value) return value;

    value = new Date(value);

    let hour = Time.formatNumber(value.getHours());
    let minutes = Time.formatNumber(value.getMinutes());
    let seconds = Time.formatNumber(value.getSeconds());
    let mountName = Time.getMountName(value.getDate(), value.getMonth());

    return `${value.getDate()} ${mountName} ${hour}:${minutes}:${seconds}`;
}
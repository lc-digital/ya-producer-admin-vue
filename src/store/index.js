import Vue from 'vue';
import Vuex from 'vuex';
import form from "@/store/modules/form";
import user from "@/store/modules/user";
import auth from "@/store/modules/auth";
import orders from "@/store/modules/orders";
import orderStatus from "@/store/modules/order-status";
import paymentStatus from "@/store/modules/payment-status";

Vue.use(Vuex)

export default new Vuex.Store({
    modules: {
        form,
        user,
        auth,
        orders,
        orderStatus,
        paymentStatus
    }
});
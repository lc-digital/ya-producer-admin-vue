import { HTTP } from "@/common/HTTP";

export default {
    state: {
        userDetail: {}
    },
    getters: {
        userDetail(state) {
            return state.userDetail;
        }
    },
    mutations: {
        setUserDetail(state, user) {
            state.userDetail = user;
        }
    },
    actions: {
        async findUserById({ commit }, id) {
            let response = await HTTP.get(`/api/v1/users/${id}`);
            commit('setUserDetail', response.data);
        }
    }
}
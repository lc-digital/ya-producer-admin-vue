import { HTTP } from "@/common/HTTP";

export default {
    state: {
        orderStatuses: []
    },
    getters: {
        orderStatuses(state) {
            return state.orderStatuses;
        }
    },
    mutations: {
        setOrderStatuses(state, orderStatuses) {
            state.orderStatuses = orderStatuses;
        }
    },
    actions: {
        async fetchOrderStatuses({commit, getters}) {
           if(!getters.orderStatuses.length) {
               let response = await HTTP.get('/api/v1/order-statuses');
               commit('setOrderStatuses', response.data);
           }
        }
    }
}
import { HTTP } from "@/common/HTTP";

export default {
    state: {
        paymentStatuses: []
    },
    getters: {
        paymentStatuses(state) {
            return state.paymentStatuses;
        }
    },
    mutations: {
        setPaymentStatuses(state, paymentStatuses) {
            state.paymentStatuses = paymentStatuses;
        }
    },
    actions: {
        async fetchPaymentStatuses({commit, getters}) {
            if(!getters.paymentStatuses.length) {
                let response = await HTTP.get('/api/v1/payment-statuses');
                commit('setPaymentStatuses', response.data);
            }
        }
    }
}
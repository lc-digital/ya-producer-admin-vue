import Vue from 'vue';
import VueRouter from 'vue-router';

Vue.use(VueRouter)

const routes = [{
        path: '/',
        name: 'Home',
        component: () =>
            import ("../views/Home"),
        meta: {
            auth: true
        }
    },
    {
        path: '/orders',
        name: 'Orders',
        component: () =>
            import ("../views/Orders"),
        meta: {
            auth: true
        }
    },
    {
        path: '/orders/:id',
        name: 'OrderDetail',
        component: () =>
            import ('../views/OrderDetail'),
        meta: {
            auth: true
        }
    },
    {
        path: '/users/:id',
        name: 'UserDetail',
        component: () =>
            import ("../views/UserDetail"),
        meta: {
            auth: true
        }
    },
    {
        path: '/login',
        name: 'Login',
        component: () =>
            import ("../views/Login"),
        meta: {
            guest: true
        }
    }
]

const router = new VueRouter({
    mode: 'history',
    base: process.env.BASE_URL,
    routes
});

router.beforeEach((to, from, next) => {
    if (to.meta.guest) {
        if (localStorage.getItem('token')) {
            next('/orders');
        }
    } else if (to.meta.auth) {
        if (!localStorage.getItem('token')) {
            next('/login');
        }
    }

    next();
});

export default router
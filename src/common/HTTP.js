import Axios from "axios";

export const HTTP = Axios.create({
    baseURL: process.env.VUE_APP_API_BASE_URL,
    headers: {
        Authorization: `bearer ${localStorage.getItem('token')}`
    }
});
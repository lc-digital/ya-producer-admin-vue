export default {
    get: () => {
        return Math.floor(Date.now() / 1000);
    },
    getMountName: (day, mountNumber) => {
        let names = [
            'Январь', 'Февраль', 'Март', 'Апрель', 'Май',
            'Июнь', 'Июль', 'Август', 'Сентябрь', 'Октябрь',
            'Ноябрь', 'Декабрь'
        ];

        return names[mountNumber];
    },
    formatNumber: number => {
        if (+number >= 0 && +number <= 9) {
            return `0${number}`;
        }
        return number;
    }
}
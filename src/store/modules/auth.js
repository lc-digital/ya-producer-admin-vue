import Time from '@/helpers/time';
import { HTTP } from "@/common/HTTP";

export default {
    state: {
        token: localStorage.getItem('token'),
        expired_in: localStorage.getItem('expired_in')
    },
    getters: {
        isAuth(state) {
            return state.token !== null;
        }
    },
    mutations: {
        setAuth(state, auth) {
            localStorage.setItem('token', auth.access_token);
            localStorage.setItem('expired_in', Time.get() + +auth.expires_in);
        }
    },
    actions: {
        async login({ commit }, formData) {
            let response = await HTTP.post('/api/v1/auth/login', formData);
            commit('setAuth', response.data);
        }
    }
}